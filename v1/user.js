var express = require('express');
var router = express.Router();

router.get("/", function(request, response)
{
    response.json({
        'success': true,
        'usuario': {
            'email': request.user_data.email,
            'nombre': request.user_data.nombre,
            'permisos': []
        }
    });
});

module.exports = router;
