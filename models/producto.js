var mongoose = require('mongoose');

var schemaProducto = new mongoose.Schema({
    "codigo": {
        type: String,
        required: [true, "Ingrese un código de producto"],
        index: {
            unique: true
        }
    },
    "nombre": {
        type: String,
        required: [true, "El nombre de producto no puede estar vacío"]
    },
    "precio": {
        type: Number,
        required: [true, "Se debe especificar el precio del producto"]
    }
});

module.exports = mongoose.model("Producto", schemaProducto);
