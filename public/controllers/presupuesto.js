'use strict';

angular.module("lien")

.controller("crearpresupuesto", [
    "$scope",
    "$rootScope",
    "$location",
    "$http",
    "ngDialog",

    function($scope, $rootScope, $location, $http, ngDialog)
    {
        $rootScope.accion = "Crear presupuesto";

        $rootScope.presupuesto = {
            cliente: {},
            productos: []
        };

        $scope.cambiar_cliente = function()
        {
            ngDialog.open({
                template: 'views/clientes.modal.html',
                className: 'ngdialog-theme-default',
                showClose: false,
                controller: ['$http', '$scope', '$rootScope', function($http, $scope) {
                    $scope.modo = "seleccion";

                    $scope.clientes =
                    {
                        q:'',
                        anterior:'',
                        resultados:[],
                        seleccionado:0,
                        seleccionar:function()
                        {
                            $rootScope.presupuesto.cliente = $scope.clientes.resultados[$scope.clientes.seleccionado];
                            ngDialog.close();
                        },
                        buscar:function(tecla)
                        {
                            if (tecla == 40)
                            {
                                $scope.clientes.seleccionado = ($scope.clientes.seleccionado == ($scope.clientes.resultados.length - 1)) ? 0 : $scope.clientes.seleccionado + 1;
                                return;
                            }

                            if (tecla == 38)
                            {
                                $scope.clientes.seleccionado = ($scope.clientes.seleccionado == 0) ? ($scope.clientes.resultados.length - 1) : $scope.clientes.seleccionado - 1;
                                return;
                            }

                            if (tecla == 13 && $scope.clientes.resultados.length > 0)
                            {
                                $scope.clientes.seleccionar();
                                return;
                            }

                            if ($scope.clientes.q.trim() == '' || $scope.clientes.anterior.indexOf($scope.clientes.q) == 0)
                            {
                                return;
                            }

                            $scope.clientes.resultados = [];

                            $http.get("/v1/cliente/busqueda/" + $scope.clientes.q).success(function(r)
                            {
                                if (r.success && r.resultados.length > 0)
                                {
                                    $scope.clientes.resultados = r.resultados;
                                }
                            });
                        }
                    }
                }]
            });
        };

        $scope.buscar_productos = function()
        {
            //...
        };

        $scope.cambiar_cliente();
    }
]);
