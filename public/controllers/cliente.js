'use strict';

angular.module("lien")

.controller("nuevocliente", ['$scope','$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Agregar cliente";

    $scope.cargar_cliente = function()
    {
        $http.post('/v1/cliente', $scope.form).success(function(r)
        {
            if (r.success)
            {
                $location.path('/cliente/' + r.cliente_id);
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };
}])

.controller("vercliente", [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    function($scope, $rootScope, $http, $location)
    {
        $rootScope.accion = "Ver cliente";

        $scope.editar = function()
        {
            $scope.editando = true;

            $('input').removeAttr('readonly');
        };

        $scope.guardar_cambios = function()
        {
            if (!$scope.editando)
            {
                return;
            }

            $http.put("/v1" + $location.path(), $scope.form).success(function(r) {
                if (r.success)
                {
                    $scope.cancelar();
                }
                else
                {
                    $scope.errores = r.mensaje;
                }
            });
        };

        $scope.borrar = function()
        {
            $location.path("/cliente/eliminar/" + $scope.form._id);
        };

        $scope.cancelar = function()
        {
            $http.get('/v1' + $location.path()).success(function(r) {
                $scope.editando = false;

                if (!r.success)
                {
                    $scope.error = r.mensaje;
                    return;
                }

                $scope.form = r.cliente;
                $('input').attr('readonly','true');
            });
        };

        $scope.cancelar();
    }
])

.controller("borrarcliente", ['$scope', '$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Eliminar cliente";

    $scope.cliente = {};

    $http.get("/v1" + $location.path().replace('/eliminar', '')).success(function(r) {
        if (r.success)
        {
            $scope.cliente = r.cliente;
        }
    });

    $scope.cancelar = function()
    {
        $location.path('/cliente/listado');
    };

    $scope.borrar = function()
    {
        $http.delete("/v1" + $location.path().replace('/eliminar', '')).success(function(r) {
            if (r.success)
            {
                $location.path('/cliente/listado');
            }
            else
            {
                $scope.error = r.mensaje;
            }
        });
    }
}])

.controller("listarclientes", ['$scope','$rootScope','$http','$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Listado de clientes";

    $scope.clientes = [];

    $http.get("/v1/cliente").success(function(r) {
        if (r.success && r.clientes.length > 0)
        {
            $scope.clientes = r.clientes;
        }
    })
}]);
