var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

var Movimiento = require('./models/movimiento.js');
var Producto = require('./models/producto.js');

router.post("/", function(request, response) {
    var mov = new Movimiento(request.body);

    // TODO: Actualizar stock
    mov.save(function(err, insertado) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'insertado': insertado
            });
        }
    });
});

module.exports = router;
