'use strict';

angular.module("lien")

.controller("ajustes", ['$scope','$localStorage', '$rootScope', function($scope, $localStorage, $rootScope) {
    $scope.vars = {
        targets:
        [
            {
                menu: "Nuevo presupuesto",
                val: "presupuesto",
                inicial: '_blank'
            },
            {
                menu: "Nuevo pedido",
                val: "pedido",
                inicial: '_blank'
            },
            {
                menu: "Nuevo remito",
                val: "remito",
                inicial: '_blank'
            },
            {
                menu: "Nueva factura",
                val: "factura",
                inicial: '_blank'
            }
        ],
        temas:
        [
            {
                nombre: "Predeterminado",
                desc: "El estilo predeterminado de la aplicación",
                css: "default.css"
            },
            {
                nombre: "Oscuro",
                desc: "Un tema oscuro y minimalista",
                css: "dark.css"
            }
        ]
    };
}])

.controller("config", ["$scope", "$rootScope", "$http", function($scope, $rootScope, $http) {
    $('#saved').hide();

    $http.get("/v1/config").success(function(r)
    {
        $scope.config = (r.success) ? r.config : [];
    });

    $http.get("/v1/marca").success(function(r)
    {
        $scope.marcas = (r.success) ? r.marcas : [];
    });

    $http.get("/v1/categoria").success(function(r)
    {
        $scope.categorias = (r.success) ? r.categorias : [];
    });

    $scope.guardar = function()
    {
        $http.put("/v1/config", $scope.config).success(function(r) {
            if (r.success)
            {
                $('#saved').show().delay(3000).fadeOut('fast');
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };

    $scope.agregar_categoria = function()
    {
        $http.post("/v1/categoria", angular.copy($scope.add_categoria)).success(function(r) {
            if (r.success)
            {
                $scope.categorias.push(r.categoria);
                $scope.add_categoria.nombre = '';
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };

    $scope.borrar_categoria = function(num)
    {
        $http.delete("/v1/categoria/" + $scope.categorias[num]._id).success(function(r) {
            if (r.success)
            {
                $scope.categorias.splice(num, 1);
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };

    $scope.agregar_marca = function()
    {
        $http.post("/v1/marca", angular.copy($scope.add_marca)).success(function(r) {
            if (r.success)
            {
                $scope.marcas.push(r.marca);
                $scope.add_marca.nombre = '';
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };

    $scope.borrar_marca = function(num)
    {
        $http.delete("/v1/marca/" + $scope.marcas[num]._id).success(function(r) {
            if (r.success)
            {
                $scope.marcas.splice(num, 1);
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };
}]);
