'use strict';

angular.module("lien")

.controller("nuevoproducto", ['$scope','$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Agregar producto";

    $scope.form = {
        categorias: [],
        descuentos: []
    };

    $scope.marca_seleccionada = '';

    $scope.set_marca = function(id, nombre)
    {
        $scope.form.marca = id;
        $scope.marca_seleccionada = nombre;
    };

    $scope.cargar_producto = function()
    {
        $http.post('/v1/producto', $scope.form).success(function(r)
        {
            if (r.success)
            {
                $location.path('/producto/' + r.producto_id);
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };

    $scope.alternar_categoria = function(id)
    {
        var pos = $scope.form.categorias.indexOf(id);

        if (pos > -1)
        {
            $scope.form.categorias.splice(pos, 1);
        }
        else
        {
            $scope.form.categorias.push(id);
        }
    };

    $http.get("/v1/marca").success(function(r)
    {
        $scope.marcas = (r.success) ? r.marcas : [];
    });

    $http.get("/v1/categoria").success(function(r)
    {
        $scope.categorias = (r.success) ? r.categorias : [];
    });
}])

.controller("verproducto", [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    function($scope, $rootScope, $http, $location)
    {
        $rootScope.accion = "Ver producto";

        $scope.editar = function()
        {
            $scope.editando = true;

            $('input').removeAttr('readonly');
        };

        $scope.guardar_cambios = function()
        {
            if (!$scope.editando)
            {
                return;
            }

            $http.put("/v1" + $location.path(), $scope.form).success(function(r) {
                if (r.success)
                {
                    $scope.cancelar();
                }
                else
                {
                    $scope.errores = r.mensaje;
                }
            });
        };

        $scope.borrar = function()
        {
            $location.path("/producto/eliminar/" + $scope.form._id);
        };

        $scope.cancelar = function()
        {
            $http.get('/v1' + $location.path()).success(function(r) {
                $scope.editando = false;

                if (!r.success)
                {
                    $scope.error = r.mensaje;
                    return;
                }

                $scope.form = r.producto;
                $('input').attr('readonly','true');
            });
        };

        $scope.cancelar();
    }
])

.controller("borrarproducto", ['$scope', '$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Eliminar producto";

    $http.get("/v1" + $location.path().replace('/eliminar', '')).success(function(r) {
        if (r.success)
        {
            $scope.producto = r.producto;
        }
    });

    $scope.cancelar = function()
    {
        $location.path('/producto/listado');
    };

    $scope.borrar = function()
    {
        $http.delete("/v1" + $location.path().replace('/eliminar', '')).success(function(r) {
            if (r.success)
            {
                $location.path('/producto/listado');
            }
            else
            {
                $scope.error = r.mensaje;
            }
        });
    }
}])

.controller("listarproductos", ['$scope','$rootScope','$http','$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Listado de productos";

    $scope.productos = [];

    $http.get("/v1/producto").success(function(r) {
        if (r.success && r.productos.length > 0)
        {
            $scope.productos = r.productos;
        }
    })
}]);
