angular.module("lien-login", ['ngStorage'])

.controller("Login", ["$scope","$http", "$localStorage", function($scope, $http, $localStorage) {
    $scope.login = function()
    {
        $http.post("/auth", $scope.form).success(function(r)
        {
            if (r.success)
            {
                $localStorage["token"] = r.token;
                window.location = "/";
            }
            else
            {
                $scope.error = r.mensaje;
            }
        })
    };
}]);
