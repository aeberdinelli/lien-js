'use strict';

angular.module("lien")

.controller("nuevoproveedor", ['$scope','$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Agregar proveedor";

    $scope.cargar_proveedor = function()
    {
        $http.post('/v1/proveedor', $scope.form).success(function(r)
        {
            if (r.success)
            {
                $location.path('/proveedor/' + r.proveedor_id);
            }
            else
            {
                $scope.errores = r.mensaje;
            }
        });
    };
}])

.controller("verproveedor", [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    function($scope, $rootScope, $http, $location)
    {
        $rootScope.accion = "Ver proveedor";

        $scope.editar = function()
        {
            $scope.editando = true;

            $('input').removeAttr('readonly');
        };

        $scope.guardar_cambios = function()
        {
            if (!$scope.editando)
            {
                return;
            }

            $http.put("/v1" + $location.path(), $scope.form).success(function(r) {
                if (r.success)
                {
                    $scope.cancelar();
                }
                else
                {
                    $scope.errores = r.mensaje;
                }
            });
        };

        $scope.borrar = function()
        {
            $location.path("/proveedor/eliminar/" + $scope.form._id);
        };

        $scope.cancelar = function()
        {
            $http.get('/v1' + $location.path()).success(function(r) {
                $scope.editando = false;

                if (!r.success)
                {
                    $scope.error = r.mensaje;
                    return;
                }

                $scope.form = r.proveedor;
                $('input').attr('readonly','true');
            });
        };

        $scope.cancelar();
    }
])

.controller("borrarproveedor", ['$scope', '$rootScope', '$http', '$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Eliminar proveedor";

    $scope.proveedor = {};

    $http.get("/v1" + $location.path().replace('/eliminar', '')).success(function(r) {
        if (r.success)
        {
            $scope.proveedor = r.proveedor;
        }
    });

    $scope.cancelar = function()
    {
        $location.path('/proveedor/listado');
    };

    $scope.borrar = function()
    {
        $http.delete("/v1" + $location.path().replace('/eliminar', '')).success(function(r) {
            if (r.success)
            {
                $location.path('/proveedor/listado');
            }
            else
            {
                $scope.error = r.mensaje;
            }
        });
    }
}])

.controller("listarproveedores", ['$scope','$rootScope','$http','$location', function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Listado de proveedors";

    $scope.proveedors = [];

    $http.get("/v1/proveedor").success(function(r) {
        if (r.success && r.proveedors.length > 0)
        {
            $scope.proveedors = r.proveedors;
        }
    })
}]);
