var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var Cliente = require('./models/cliente.js');

var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.post("/", function(request, response, next)
{
    var nuevo = new Cliente(request.body);

    nuevo.save(function(err, insertado) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err.errors
            });
        }
        else
        {
            response.json({
                'success': true,
                'cliente_id': insertado._id
            });
        }
    });
});

router.get("/", function(request, response, next)
{
    Cliente.find({}, function(err, resultados) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'clientes': resultados
            });
        }
    })
});

router.get("/:id", function(request, response, next)
{
    Cliente.find({
        '_id': request.params.id
    }, function(err, cliente){
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            if (cliente.length > 0)
            {
                response.json({
                    'success': true,
                    'cliente': cliente[0]
                });
            }
            else
            {
                response.json({
                    'success': false,
                    'mensaje': 'No se encontró el cliente'
                });
            }
        }
    });
});

router.get("/busqueda/:s", function(request, response) {
    var r = new RegExp("^" + request.params.s, "i");

    Cliente.find({
        'nombre': r
    }, function(err, resultados) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'resultados': resultados
            });
        }
    })
});

router.delete("/:id", function(request, response, next)
{
    Cliente.remove({
        '_id': request.params.id
    }, function(err, data){
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

router.put("/:id", function(request, response, next)
{
    Cliente.findOneAndUpdate({
        '_id': request.params.id
    }, request.body, function(err, data) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

module.exports = router;
