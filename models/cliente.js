var mongoose = require('mongoose');

var schemaCliente = new mongoose.Schema({
    "nombre": {
        type: String,
        required: [true, "El nombre de cliente no puede estar vacío"]
    },
    "telefono": {
        type: String,
        required: [true, "Se debe especificar un número de teléfono"]
    },
    "domicilio": {
        type: String,
        required: [true, "El domicilio es obligatorio"]
    },
    "localidad": {
        type: String,
        required: [true, "La localidad es obligatoria"]
    },
    "iva": {
        type: String,
        required: [true, "No se definió el tipo de IVA"],
        default: "Consumidor Final"
    },
    "movil": {
        type: String
    },
    "email": {
        type: String
    },
    "identificacion": {
        type: Number,
        required: [true, "Se debe especificar un DNI o CUIT"]
    }
});

module.exports = mongoose.model("Cliente", schemaCliente);
