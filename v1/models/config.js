var mongoose = require('mongoose');

var schemaConfig = new mongoose.Schema({
    "variable": String,
    "valor": String,
    "tipo": {
        type: String,
        enum: ["text", "number", "email", "tel", "password"]
    },
    "titulo": String
});

module.exports = mongoose.model("Config", schemaConfig);
