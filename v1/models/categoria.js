var mongoose = require('mongoose');

var schemaCategoria = new mongoose.Schema({
    "nombre": {
        type: String,
        required: [true, "El nombre de la categoría no puede estar vacío"]
    }
});

module.exports = mongoose.model("Categoria", schemaCategoria);
