var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var Proveedor = require('./models/proveedor.js');

var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.post("/", function(request, response, next)
{
    var nuevo = new Proveedor(request.body);

    nuevo.save(function(err, insertado) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err.errors
            });
        }
        else
        {
            response.json({
                'success': true,
                'proveedor_id': insertado._id
            });
        }
    });
});

router.get("/", function(request, response, next)
{
    Proveedor.find({}, function(err, resultados) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'proveedors': resultados
            });
        }
    })
});

router.get("/:id", function(request, response, next)
{
    Proveedor.find({
        '_id': request.params.id
    }, function(err, proveedor) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            if (proveedor.length > 0)
            {
                response.json({
                    'success': true,
                    'proveedor': proveedor[0]
                });
            }
            else
            {
                response.json({
                    'success': false,
                    'mensaje': 'No se encontró el proveedor'
                });
            }
        }
    });
});

router.delete("/:id", function(request, response, next)
{
    Proveedor.remove({
        '_id': request.params.id
    }, function(err, data){
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

router.put("/:id", function(request, response, next)
{
    Proveedor.findOneAndUpdate({
        '_id': request.params.id
    }, request.body, function(err, data) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

module.exports = router;
