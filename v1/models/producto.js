var mongoose = require('mongoose');

var schemaProducto = new mongoose.Schema({
    "codigo": {
        type: String,
        required: [true, "Ingrese un código de producto"],
        unique: true
    },
    "nombre": {
        type: String,
        required: [true, "El nombre de producto no puede estar vacío"]
    },
    "precio": {
        type: Number,
        required: [true, "Se debe especificar el precio del producto"]
    },
    "descuentos": {
        type: [Number],
        default: []
    },
    "stock": {
        type: Number,
        default: 0
    },
    "marca": {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Marca',
        required: [true, "Se debe especificar una marca"]
    },
    "categorias": [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Categoria'
    }],
    "ean13": {
        type: String
    }
});

module.exports = mongoose.model("Producto", schemaProducto);
