'use strict';

angular.module('lien', [
    'ngRoute',
    'ngDialog',
    'ngAnimate',
    'ngStorage',
    'lien.router'
])

.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider.otherwise({
        redirectTo: '/'
    });
}])

.controller('Launch', ['$scope', '$rootScope', '$localStorage', '$http', function($scope, $rootScope, $localStorage, $http) {
    $rootScope.accion = "Inicio";

    $http.get("/v1/config/empresa").success(function(r) {
        $rootScope.empresa = (r != '') ? r : 'Lien Sistemas';
    });

    $localStorage.$default({
        ajustes:{
            target: {
                "presupuesto": "_blank",
                "pedido": "",
                "remito": "",
                "factura": "",
                "add-producto": "_blank",
                "productos": "",
                "add-cliente": "_blank",
                "clientes": "",
                "add-proveedor": "_blank",
                "proveedor": ""
            },
            theme: "default.css"
        }
    });

    $rootScope.ajustes = $localStorage.ajustes;

    // Aplicacion de escritorio?
    if (window && window.process && window.process.type)
    {
        $('html').addClass('electron');
    }

    // Forzar autenticacion
    if (typeof $localStorage["token"] == 'undefined' || $localStorage["token"] == '')
    {
        window.location = '/auth';
        return;
    }

    $http.defaults.headers.common['x-access-token'] = $localStorage["token"];

    // Obtener la informacion del usuario
    $http.get('/v1/user').success(function(r) {
        if (r.success)
        {
            $rootScope.user = r.usuario;
        }
        else
        {
            window.location = '/auth';
        }
    });
}])

.animation('.view', function() {
    return {
        enter:function(obj, done)
        {
            obj.css('display','none').fadeIn(500, done);
            return function(){obj.stop()}
        },
        leave:function(obj, done)
        {
            obj.css('display','none');
            return function(){obj.stop()}
        }
    };
});
