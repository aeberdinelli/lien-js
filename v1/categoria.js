var express = require('express');
var router = express.Router();

var Categoria = require('./models/categoria.js');

router.get("/", function(request, response) {
    Categoria.find({}, function(err, resultados) {
        if (err)
        {
            response.send({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.send({
                'success': true,
                'categorias': resultados
            });
        }
    });
});

router.post("/", function(request, response) {
    var nueva = new Categoria(request.body);
    nueva.save(function(err, saved) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'categoria': saved
            });
        }
    })
});

router.delete("/:id", function(request, response) {
    Categoria.remove({
        '_id': request.params.id
    }, function(err, data) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

module.exports = router;
