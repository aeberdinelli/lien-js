# Lien Sistemas
Lien Sistemas ERP es el futuro mejor sistema de gestión empresarial desarrollado para Argentina.

Implementando las tecnologías más recientes, como [Node.js](https://nodejs.org), [MongoDB](http://mongodb.org/), [AngularJS](http://angularjs.org/), [Bootstrap](http://getbootstrap.com/), entre otras, promete ser rápido, flexible y potente.

### Funciones
* `/v1` contiene la API Rest completa
* `/public` contiene la interfaz web desarrollada con AngularJS, jQuery y Bootstrap
* `/clients` contiene diferentes aplicaciones para utilizar la API
  * `/desktop` contiene la aplicación de escritorio desarrollada con Electron. Es compatible con Windows, Linux y Mac OS X.
  * `/android` *(proximamente)* App Android desarrollado con PhoneGap o Ionic
  * `/ios` *(proximamente)* App para iOS desarrollado con PhoneGap

### Instrucciones
###### Para iniciar la API
1. Clonar el repositorio
2. Modificar las preferencias en el archivo `/config.json`. **Especialmente** la variable `secret`.
3. Ejecutar en la raiz del proyecto `npm install && npm start`

###### Para abrir la aplicacion de escritorio
1. Clonar el repositorio
2. Ingresar a `/clients/desktop`
  * Si se modificó el puerto en `config.json` también se debe actualizar la URL en el archivo `main.js`.
3. Ejecutar `npm install && npm start`
4. Se puede crear un acceso directo escribiendo esa última línea en un archivo `.bat`

### Código abierto
La aplicación es completamente *open source* por lo que podés adaptarla a tus necesidades.
Cualquier colaboración mediante código, donación o simplemente compartiendo es bienvenida :)
