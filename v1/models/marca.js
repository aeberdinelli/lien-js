var mongoose = require('mongoose');

var schemaMarca = new mongoose.Schema({
    "nombre": {
        type: String,
        required: [true, "El nombre de la marca no puede estar vacía"]
    }
});

module.exports = mongoose.model("Marca", schemaMarca);
