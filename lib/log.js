var mongoose = require('mongoose');
var Log = require('../models/log.js');

module.exports = function(user, info)
{
    var log = new Log({
        'administrador': user.nombre,
        'accion': info
    });

    log.save(function(err, data) {
        if (err)
        {
            console.log("[No se pudo registrar] " + info);
        }
    });
};
