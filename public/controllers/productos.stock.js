angular.module("lien")

.controller("stock", ["$scope", "$rootScope", "$http", "$location", function($scope, $rootScope, $http, $location) {
    $rootScope.accion = "Control de stock";

    $scope.movimiento = {
        motivo: '',
        ingresos: [],
        egresos: []
    };

    $scope.ag = function(tipo)
    {
        $scope.movimiento[tipo + 's'].push(angular.copy($scope.agregar));

        $scope.agregar = {
            codigo: '',
            descripcion: '',
            cantidad: 0
        };
    };

    $scope.guardar = function()
    {
        $http.post("/v1/stock", $scope.movimiento).success(function(r)
        {
            if (r.success)
            {
                $location.path("/producto/listado");
            }
            else
            {
                $scope.error = r.mensaje;
            }
        });
    };
}]);
