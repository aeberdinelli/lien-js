var config      = require('../config.json');
var express     = require('express');
var router      = express.Router();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var jwt         = require('jwt-simple');
var mongoose    = require('mongoose');
var moment      = require('moment');

var productos   = require('./producto.js');
var clientes    = require('./cliente.js');
var proveedores = require('./proveedor.js');
var users       = require('./user.js');
var marcas      = require('./marca.js');
var categorias  = require('./categoria.js');
var movimientos = require('./stock.js');
var config      = require('./config.js');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.use("/user", users);
router.use("/producto", productos);
router.use("/cliente", clientes);
router.use("/proveedor", proveedores);
router.use("/marca", marcas);
router.use("/categoria", categorias);
router.use("/stock", movimientos);
router.use("/config", config);

module.exports = router;
