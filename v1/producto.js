var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var Producto = require('./models/producto.js');

var router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.post("/", function(request, response, next)
{
    var nuevo = new Producto(request.body);

    nuevo.save(function(err, insertado) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err.errors
            });
        }
        else
        {
            response.json({
                'success': true,
                'producto_id': insertado._id
            });
        }
    });
});

router.get("/", function(request, response, next)
{
    Producto.find({}, function(err, resultados) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'productos': resultados
            });
        }
    })
});

router.get("/:id", function(request, response, next)
{
    Producto.find({
        '_id': request.params.id
    }, function(err, producto) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            if (producto.length > 0)
            {
                response.json({
                    'success': true,
                    'producto': producto[0]
                });
            }
            else
            {
                response.json({
                    'success': false,
                    'mensaje': 'No se encontró el producto'
                });
            }
        }
    });
});

router.delete("/:id", function(request, response, next)
{
    Producto.remove({
        '_id': request.params.id
    }, function(err, data) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

router.put("/:id", function(request, response, next)
{
    Producto.findOneAndUpdate({
        '_id': request.params.id
    }, request.body, function(err, data) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

module.exports = router;
