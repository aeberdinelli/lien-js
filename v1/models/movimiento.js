var mongoose = require('mongoose');

var schemaMovimiento = new mongoose.Schema({
    "motivo": {
        type: String,
        required: [true, "Especifica un motivo"]
    },
    "ingresos": [],
    "egresos": []
});

module.exports = mongoose.model("Movimiento", schemaMovimiento);
