var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var SALT_WORK_FACTOR = 10;

var schemaUsuario = new mongoose.Schema({
    'nombre': {
        type: String,
        required: [true, "Ingrese el nombre del usuario"],
        index: {
            unique: true
        }
    },
    'email': {
        type: String,
        required: [true, "Ingrese un correo electrónico de contacto ya que éste se utiliza para iniciar sesión"]
    },
    'permisos': {
        type: [String]
    },
    'passwd': {
        type: String,
        required: true
    },
    "master": {
        type: Boolean,
        required: true,
        default: false
    }
});

// Adaptado desde http://blog.mongodb.org/post/32866457221/password-authentication-with-mongoose-part-1
schemaUsuario.pre('save', function(next) {
    var user = this;

    if (!user.isModified('passwd'))
    {
        return next();
    }

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.passwd, salt, null, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.passwd = hash;
            next();
        });
    });
});

schemaUsuario.methods.verificar_passwd = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.passwd, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('Usuario', schemaUsuario);
