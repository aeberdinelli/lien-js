'use strict';

angular.module("lien.router", ['ngRoute','ngStorage'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: "views/_about.html",
        controller: "Launch"
    })
    .when('/salir', {
        templateUrl: "views/_logout.html",
        controller: "LogoutCtrl"
    })
    // Productos
    .when('/producto/nuevo', {
        templateUrl: "views/producto.nuevo.html",
        controller: "nuevoproducto"
    })
    .when('/producto/eliminar/:producto_id', {
        templateUrl: "views/producto.borrar.html",
        controller: "borrarproducto"
    })
    .when('/producto/listado', {
        templateUrl: "views/productos.html",
        controller: "listarproductos"
    })
    .when('/producto/movimiento', {
        templateUrl: "views/producto.stock.html",
        controller: "stock"
    })
    .when('/producto/:producto_id', {
        templateUrl: "views/producto.editar.html",
        controller: "verproducto"
    })
    // Clientes
    .when('/cliente/nuevo', {
        templateUrl: "views/cliente.nuevo.html",
        controller: "nuevocliente"
    })
    .when('/cliente/eliminar/:producto_id', {
        templateUrl: "views/cliente.borrar.html",
        controller: "borrarcliente"
    })
    .when('/cliente/listado', {
        templateUrl: "views/clientes.html",
        controller: "listarclientes"
    })
    .when('/cliente/:producto_id', {
        templateUrl: "views/cliente.editar.html",
        controller: "vercliente"
    })
    // Proveedores
    .when('/proveedor/nuevo', {
        templateUrl: "views/proveedor.nuevo.html",
        controller: "nuevoproveedor"
    })
    .when('/proveedor/eliminar/:producto_id', {
        templateUrl: "views/proveedor.borrar.html",
        controller: "borrarproveedor"
    })
    .when('/proveedor/listado', {
        templateUrl: "views/proveedores.html",
        controller: "listarproveedores"
    })
    .when('/proveedor/:producto_id', {
        templateUrl: "views/proveedor.editar.html",
        controller: "verproveedor"
    })
    // Presupuestos
    .when("/presupuesto/nuevo", {
        templateUrl: "views/presupuesto.editor.html",
        controller: "crearpresupuesto"
    })
    // Configuracion
    .when("/config/preferencias", {
        templateUrl: "views/_ajustes.html",
        controller: "ajustes"
    })
    .when("/config/sistema", {
        templateUrl: "views/_config.html",
        controller: "config"
    })
}])

.controller('LogoutCtrl', ['$scope', '$localStorage', function($scope, $localStorage) {
    delete $localStorage.token;
    //window.location = "/auth";
}]);
