var config = require('./config.json');

var morgan = require('morgan');
var bodyParser = require('body-parser');
var jwt = require('jwt-simple');

var express = require('express');
var app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/' + config.db);

var Usuario = require('./v1/models/usuario.js');

if (config.debug)
{
    app.use(morgan('dev'));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use("/v1", function(request, response, next)
{
    if (!request.headers["x-access-token"])
    {
        response.status(403).json({
            'success': false,
            'mensaje': 'No se envió el token'
        });
    }
    else
    {
        token_sent = request.headers["x-access-token"];

        try
        {
            var token = jwt.decode(token_sent, config.secret);

            if (!token)
            {
                return response.status(403).json({
                    'success': false,
                    'message': 'Los datos de autenticación son inválidos'
                });
            }
            else
            {
                Usuario.findOne({
                    '_id': token.user_id
                }, function(err, user) {
                    if (err || user == null)
                    {
                        return response.status(403).json({
                            'success': false,
                            'message': 'El token provisto es inválido'
                        });
                    }

                    request.user_data = user;
                    next();
                });
            }
        }
        catch (e)
        {
            console.log(e);
        }
    }
});

app.use("/auth", require("./lib/auth.js"));
app.use("/v1", require("./v1/router.js"));
app.use("/", express.static('public'));

app.get("/*", function(request, response)
{
    response.sendFile(__dirname + '/public/index.html');
});

app.listen(config.puerto, function(){
    console.log(
          "=========================================\n"
        + "           Lien Sistemas ERP             \n"
        + "=========================================\n"
        + "- Modo de desarrollo " + ((config.debug) ? "activado" : "desactivado") + "\n"
        + "- Conectado a base de datos " + config.db + "\n"
        + "- API sirviendo en " + config.puerto + "\n"
    );
});
