const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow

let main
let frontend = "http://localhost:3000/";

function inicializar() {
    main = new BrowserWindow({
        width: 1000,
        height: 800,
        minWidth: 1000,
        minHeight: 800,
        resizable: true,
        closable: true,
        minimizable: true,
        maximizable: true,
        center: true,
        //alwaysOnTop: true,
        icon: __dirname + '/obj/256x256.ico',
        title: "Lien Sistemas"
    });

    main.loadURL(frontend);

    //main.webContents.openDevTools()

    main.on('closed', function() {
        main = null
    });
};

app.on('ready', inicializar);

app.on('window-all-closed', function() {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('browser-window-created', function(e, ventana) {
    ventana.setMenu(null);
});

app.on('activate', function() {
    if (main === null) {
        inicializar()
    }
});
