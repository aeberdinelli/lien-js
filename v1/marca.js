var express = require('express');
var router = express.Router();

var Marca = require('./models/marca.js');

router.get("/", function(request, response) {
    Marca.find({}, function(err, resultados)
    {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'marcas': resultados
            });
        }
    })
});

router.post("/", function(request, response) {
    var nueva = new Marca(request.body);
    nueva.save(function(err, saved) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'marca': saved
            });
        }
    })
});

router.delete("/:id", function(request, response) {
    Marca.remove({
        '_id': request.params.id
    }, function(err, data) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true
            });
        }
    });
});

module.exports = router;
