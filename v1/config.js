var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();

var Config = require('./models/config.js');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

router.get("/", function(request, response) {
    Config.find({}, function(err, resultados) {
        if (err)
        {
            response.json({
                'success': false,
                'mensaje': err
            });
        }
        else
        {
            response.json({
                'success': true,
                'config': resultados
            });
        }
    });
});

router.get("/:variable", function(request, response) {
    Config.findOne({
        'variable': request.params.variable
    }, function(err, item)
    {
        if (err || typeof item == 'undefined')
        {
            response.send('');
        }
        else
        {
            response.send(item.valor);
        }
    })
});

router.put("/", function(request, response) {
    var errores = [];

    request.body.forEach(function(e,i)
    {
        var data = {
            "variable": e.variable,
            "valor": e.valor,
        };

        Config.findOneAndUpdate({
            '_id': e._id
        }, data, function(err, updated)
        {
            if (err)
            {
                errores.push(err);
            }
        });
    });

    if (errores.length > 0)
    {
        response.json({
            'success': false,
            'mensaje': errores
        });
    }
    else
    {
        response.json({
            'success': true
        });
    }
});

module.exports = router;
