// Dependencias
var config = require('../config.json');
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var jwt = require('jwt-simple');
var mongoose = require('mongoose');
var moment = require('moment');

// Schemas
var Usuario = require('../v1/models/usuario.js');

// Init
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));

if (config.debug)
{
    router.use(morgan('dev'));
}

router.get("/", express.static("public/login.html"));
router.post("/", function(request, response, next)
{
    Usuario.find({
        'email': request.body.email
    }, function(err, user) {
        if (err || user.length == 0)
        {
            return response.json({
                'success': false,
                'mensaje': 'No se encontró ningún usuario con ese correo'
            });
        }

        user[0].verificar_passwd(request.body.passwd, function(err, resultado)
        {
            if (err)
            {
                return response.json({
                    'success': false,
                    'mensaje': err
                });
            }

            if (resultado)
            {
                var token = jwt.encode({
                    user_id: user[0]._id
                }, config.secret);

                return response.json({
                    'success': true,
                    'token': token
                });
            }

            return response.json({
                'success': false,
                'mensaje': 'La contraseña ingresada es incorrecta'
            });
        });
    });
});

module.exports = router;
