var mongoose = require('mongoose');

var schemaCliente = new mongoose.Schema({
    "nombre": {
        type: String,
        required: [true, "El nombre de cliente no puede estar vacío"]
    },
    "telefono": {
        type: String,
        validate:
        {
            validator: function(val)
            {
                return /^[\+\(\)0-9 -]+$/.test(val);
            },
            message: "El número de teléfono solo puede contener números"
        },
        required: [true, "Se debe especificar un número de teléfono"]
    },
    "domicilio": {
        type: String,
        required: [true, "El domicilio es obligatorio"]
    },
    "localidad": {
        type: String,
        required: [true, "La localidad es obligatoria"]
    },
    "iva": {
        type: String,
        enum: ["Consumidor Final", "Responsable Inscripto", "Responsable Monotributo", "Exento"],
        required: [true, "No se definió el tipo de IVA"],
        default: "Consumidor Final"
    },
    "movil": {
        type: String,
        validate:
        {
            validator: function(val)
            {
                return /^[\+\(\)0-9 -]+$/.test(val);
            },
            message: "El número de móvil solo puede contener números"
        }
    },
    "email": {
        type: String,
        validate:
        {
            validator: function(val)
            {
                return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(val);
            },
            message: "El email ingresado es inválido"
        }
    },
    "identificacion": {
        type: Number,
        validation:
        {
            validator: function(val)
            {
                val = val.replace('-','').replace('.', ',').trim();
                return (/^[0-9]+$/.test(val) && (val.length == 8 || val.length == 11));
            },
            message: "El CUIT o DNI ingresado es inválido"
        },
        required: [true, "Se debe especificar un DNI o CUIT"]
    }
});

module.exports = mongoose.model("Cliente", schemaCliente);
